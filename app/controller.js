import Controller from 'cerebral';
import Model from 'cerebral-baobab';
import request from 'superagent';

const model = Model({
  title: 'TaskMnager',
  autor: 'Alexander Shreyner',
  visiblTask: null,
  tasks: []
});
const service = {
  request: request
};

export default Controller(model,service);
