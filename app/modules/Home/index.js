import React from 'react';
import {Decorator as Cerebral} from 'cerebral-react';
import Title from './components/Title';
import Footer from './components/Footer';
import TaskList from './components/TaskList';

@Cerebral({
  title: ['title'],
  tasks: ['tasks'],
  autor: ['autor']
})
class App extends React.Component {

  render() {

    return (
      <div>
        <Title>{this.props.title}</Title>
        <hr/>
        {
          (this.props.tasks.length)?
          <TaskList/> :
          this.props.signals.getTask()
        }
        <hr/>
        <Footer autor={this.props.autor} />
      </div>
    );
  }
}

export default App;
