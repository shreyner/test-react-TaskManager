function getTaskList (input, state, output, services) {

  services.request('/tasklist', function (err, response) {

    if (err) {
      output.error({
        error: err.toString()
      });
    } else {
      output.success({
        result: JSON.parse(response.text)
      });
    }

  });

};

export default getTaskList;
