import getTaskList from './signals/getTask';
import visiblTaskChanged from './signals/visiblTaskChanged';

export default (controller) => {


  controller.signal('getTask', getTaskList);
  controller.signal('visiblTaskChanged', visiblTaskChanged);

};
