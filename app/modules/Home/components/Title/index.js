import React from 'react';

function Title(props) {
  return (
    <h1>
      {props.children}
    </h1>
  );
}

Title.propTypes = {
  children: React.PropTypes.any.isRequired
};

export default Title;
