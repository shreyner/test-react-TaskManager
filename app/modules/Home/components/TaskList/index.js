import React from 'react';
import {Decorator as Cerebral} from 'cerebral-react';
import Task from '../Task';
import './style.css';

@Cerebral({
  tasks:['tasks']
})

class TaskList extends React.Component {

  renderTask(task,index){
    return <Task key={index} task={task} />
  }

  render(){

    return(
      <section>
        <ul className="mainList">
          {this.props.tasks.map(this.renderTask.bind(this))}
        </ul>
      </section>
    )
  }
};

export default TaskList;
