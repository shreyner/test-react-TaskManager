import React from 'react';

class Footer extends React.Component {

  render(){
    return(
      <footer>
        <p>by {this.props.autor}</p>
      </footer>
    )
  }

}

export default Footer;
