import React from 'react';
import {Decorator as Cerebral} from 'cerebral-react';
import Classnames from 'classnames';
import './style.css'

@Cerebral({
  visiblTask:['visiblTask']
})

class Task extends React.Component {

  getDate(){
    return new Date(this.props.task.DueDate).toLocaleDateString();
  }
  getLeftDate(){
    let leftDate = new Date(this.props.task.DueDate).getTime() - new Date().getTime();
    leftDate = (Math.floor(leftDate/1000))/86400;

    return Math.floor(leftDate);
  }

  getTaskStateValue(){
    switch (this.props.task.TaskStateValue) {
      case 1:
        return "Назначена";
      case 2:
        return "Выполняеться";
      case 3:
        return "Приостановлена";
      case 4:
        return "Выполнена";
      default:
        return "Sorry...";
    }
  }

  render(){
    let classNList = Classnames('liList',{
      active: (this.props.task.Id === this.props.visiblTask),
      success: this.props.task.TaskStateValue === 4,
      frozen: this.props.task.TaskStateValue === 3,
      arrears: (this.props.task.TaskStateValue < 4 && this.getLeftDate() < 0)
    });
    let classNDateleft = Classnames('default',{
      arrears:(this.props.task.TaskStateValue < 4 && this.getLeftDate() < 0)
    });

    return(
      <li className={classNList} onClick={()=>{this.props.signals.visiblTaskChanged({visiblTask:this.props.task.Id})}}>
        <div className="dateLeft">
        {
          (this.props.task.TaskStateValue === 4)?
          <div></div> :
          <div className={classNDateleft}>{Math.abs(this.getLeftDate())}</div>
        }
        </div>
        <div className="block">
          <h4 className="head">{this.props.task.Name} № {this.props.task.Number}</h4>
          Проект: {this.props.task.ProjectName}
        </div>
        <div className="right">
          {this.getTaskStateValue()} <br/>
          срок {this.getDate()}
        </div>
      </li>
    )
  }
};

export default Task;
