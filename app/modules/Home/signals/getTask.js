import getTaskList from '../actions/getTaskList';
import setTasks from '../actions/setTasks';
import resetTasks from '../actions/resetTasks';

export default [
  [
    getTaskList,{
      success:[setTasks]
    }
  ]
];
