/* eslint no-console: 0 */
import path from 'path';
import express from 'express';
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from './webpack.config.js';

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();
const TaskList = [{
    "DueDate": "2015-12-07T00:00:00Z",
    "Id": "64205c53-9aa0-443d-bb04-137c17f6f22c",
    "Name": "Задача 1.1",
    "Number": 4232,
    "ProjectName": "Проект 1",
    "TaskStateValue": 4
}, {
    "DueDate": "2015-12-08T00:00:00Z",
    "Id": "b4e6374a-68d5-4bab-b986-bf7a065628c0",
    "Name": "Задача 1.2",
    "Number": 4202,
    "ProjectName": "Проект 1",
    "TaskStateValue": 3
}, {
    "DueDate": "2015-12-09T00:00:00Z",
    "Id": "2cd75638-c488-48a8-aef8-e534eed9b43c",
    "Name": "Задача 2.1",
    "Number": 4205,
    "ProjectName": "Проект 2",
    "TaskStateValue": 2
}, {
    "DueDate": "2015-12-12T00:00:00Z",
    "Id": "8a23662e-b390-4754-8167-e9a0e86b173c",
    "Name": "Задача 2.2",
    "Number": 4233,
    "PriorityValue": 2,
    "ProjectName": "Проект 2",
    "TaskStateValue": 1
}, {
   "DueDate": "2015-12-30T00:00:00Z",
   "Id": "b4e6374a-68d5-4bab-b986-bf7a068628c0",
   "Name": "Задача 3.1",
   "Number": 4232,
   "ProjectName": "Проект 3",
   "TaskStateValue": 3
},
{
    "DueDate": "2015-12-20T00:00:00Z",
    "Id": "8a23662e-b390-4754-8167-e9a0e86b175c",
    "Name": "Задача 3.2",
    "Number": 4238,
    "PriorityValue": 2,
    "ProjectName": "Проект 3",
    "TaskStateValue": 1
},{
    "DueDate": "2015-12-21T00:00:00Z",
    "Id": "8a23662e-b390-4754-8567-e9a0e86b175c",
    "Name": "Задача 4.2",
    "Number": 4838,
    "PriorityValue": 2,
    "ProjectName": "Проект 4",
    "TaskStateValue": 4
}];

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.get('/tasklist',function (req,res) {
    res.json(TaskList)
    console.log('\nОтдал TaskList\n');
  });
  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
    res.end();
  });
} else {
  app.use(express.static(__dirname + '/dist'));
  app.get('*', function response(req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.listen(port, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }
  console.info('==> 🌎 Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});
